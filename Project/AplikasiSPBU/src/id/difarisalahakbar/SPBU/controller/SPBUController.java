/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.SPBU.controller;

import com.google.gson.Gson;
import id.difarisalahakbar.SPBU.model.SPBU;
import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author USER
 */
public class SPBUController {
    private static final String FILE = "C:\\Users\\USER\\Documents\\vegas\\AplikasiSPBU\\lib\\spbu.json";
    private SPBU spbu;
    private final Scanner in;
    private String noPlat;
    private String jenisKendaraan;
    private String Merk;
    private String jenisBensin;
    private final LocalDateTime waktuMasuk;
    private BigDecimal Biaya;
    private int Liter;
    private BigDecimal Uang;
    private final DateTimeFormatter dateTimeFormat;
    private int Pilih;
    private int HargaPerliter;
    
    public SPBUController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setPembelian() {
        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.print("Masukkan NoPol : ");
        noPlat = in.next().toUpperCase();
        System.out.println("Jenis Kendaraan : ");
        System.out.println("1. Motor");
        System.out.println("2. Mobil");
        System.out.print("Pilih Jenis Kendaraan (1/2) : ");
        Pilih = in.nextInt();
        switch (Pilih) {
            case 1 :
                jenisKendaraan = "Motor";
                break;
            case 2 :
                jenisKendaraan = "Mobil";
                break;
            default :
                System.out.println("Nomor yang ada masukan salah");
                setPembelian();                
    }
        System.out.print("Masukkan Merk Kendaraan : ");
        Merk = in.next();
        System.out.println("=================================================");
        System.out.println("Jenis BBM : ");
        System.out.println("1. Pertamax Turbo             (Rp. 10200/Liter)");
        System.out.println("2. Pertamax                   (Rp. 8600/Liter)");
        System.out.println("3. Pertalite                  (Rp. 7300/Liter)");
        System.out.println("4. Premium                    (Rp. 6500/Liter)");
        System.out.println("=================================================");
        System.out.print("Pilih Jenis BBM (1/2/3/4) : ");
        Pilih = in.nextInt();
        switch (Pilih) {
            case 1 :
                jenisBensin = "Pertamax Turbo";
                Biaya = new BigDecimal(10200);
                HargaPerliter = 10200;
                break;
            case 2 :
                jenisBensin = "Pertamax";
                Biaya = new BigDecimal(8600);
                HargaPerliter = 8600;
                break;
            case 3 :
                jenisBensin = "Pertalite";
                Biaya = new BigDecimal(7300);
                HargaPerliter = 7300;
                break;
            case 4 :
                jenisBensin = "Premium";
                Biaya = new BigDecimal(6500);
                HargaPerliter = 6500;
                break;   
            default :
                System.out.println("Nomor yang ada masukan salah");
                setPembelian();
        }   
        System.out.print("Masukkan Jumlah Liter : ");
        Liter = in.nextInt();
        Biaya = Biaya.multiply(new BigDecimal (Liter));
        System.out.println("Harga : Rp. "+ Biaya);
        spbu = new SPBU();
              spbu.setNoPlat(noPlat);
              spbu.setJenisKendaraan(jenisKendaraan);
              spbu.setMerk(Merk);
              spbu.setJenisBensin(jenisBensin);
              spbu.setLiter(Liter);
              spbu.setBiaya(Biaya);
              setWriteSPBU(FILE, spbu);
        getBayar();
        
              
        
        
    }
        
        public void getBayar(){
         System.out.print("Masukkan Jumlah Uang : Rp. ");
        Uang = in.nextBigDecimal();
        if (Uang.compareTo(Biaya) < 0){
            System.out.println("Uang anda tidak cukup!!");
            getBayar();
                    
        }else {
         
            System.out.println("Transaksi Berhasil!!");
            cetak();
        }
            System.out.println("Apakah Anda Ingin Isi BBM Kembali?");
        System.out.print("1. Ya  |  2. Tidak : ");
        Pilih = in.nextInt();
        if (Pilih == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setPembelian();
        }
        }
        
        public void cetak(){
              System.out.println("-----------------|Nota Transaksi|----------------");
              System.out.println("Jenis BBM yang dibeli adalah  : "+jenisBensin);
              System.out.println("Harga perliternya adalah        Rp."+HargaPerliter+",00");
              System.out.println("Total liter yang anda beli    : "+Liter+" liter");
              System.out.println("Harga total pembayaran        : Rp."+Biaya+",00");
              System.out.println("Uang Pembayaran               : Rp."+Uang+",00");
              Uang = Uang.subtract(Biaya);
              System.out.println("Uang Kembali                  : Rp."+Uang+",00");
              System.out.println("-------------------------------------------------");
              
              
              
        }
    
         public void setWriteSPBU(String file, SPBU spbu) {
        Gson gson = new Gson();

        List<SPBU> spbus = getReadSPBU(file);
        spbus.remove(spbu);
        spbus.add(spbu);

        String json = gson.toJson(spbus);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(SPBUController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
         public List<SPBU> getReadSPBU(String file) {
        List<SPBU> spbus = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try ( Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                SPBU[] ps = gson.fromJson(line, SPBU[].class);
                spbus.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SPBUController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SPBUController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return spbus;
    }
         public void getDataSPBU() {
        List<SPBU> spbus = getReadSPBU(FILE);
        List<SPBU> pResults = spbus.stream().collect(Collectors.toList());
        BigDecimal total = pResults.stream()
                .map(SPBU::getBiaya)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("No Polisi \tJenis Kendaraan     Merk Kendaraan \tJenis BBM \tJumlah Liter \t  Biaya");
        System.out.println("--------- \t---------------     -------------- \t--------- \t------------ \t---------");
        pResults.forEach((p) -> {
            System.out.println(p.getNoPlat() + "\t\t" + p.getJenisKendaraan() + "\t\t    " + p.getMerk() + "\t\t" + p.getJenisBensin() + "\t" + p.getLiter() + " L\t\tRp. " +  p.getBiaya());
        });
        System.out.println("--------- \t---------------     -------------- \t--------- \t------------ \t---------");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " +total);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        Pilih = in.nextInt();
        if (Pilih == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataSPBU();
        }
    }
}





    
