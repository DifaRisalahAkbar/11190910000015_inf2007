/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.SPBU.controller;

import id.difarisalahakbar.SPBU.model.Info;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println("SPBU Tangerang");
        System.out.println("-------------------------------------------------");
        System.out.println("1. Pengisian BBM ");
        System.out.println("2. Data Customer ");
        System.out.println("3. Keluar Aplikasi ");
        System.out.println("-------------------------------------------------");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        SPBUController spbu = new SPBUController();
        switch (noMenu) {
            case 1:
                spbu.setPembelian();
                break;
            case 2:
                spbu.getDataSPBU();
                break;
            case 3:
                System.out.println("goodbye everybody, i've got to go..  ;`)");
                System.exit(0);
                break;
        }
    }
    
}
