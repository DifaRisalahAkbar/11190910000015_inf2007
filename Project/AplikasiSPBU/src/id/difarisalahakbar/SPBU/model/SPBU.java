/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.SPBU.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author USER
 */
public class SPBU {

    public BigDecimal getUang() {
        return Uang;
    }

    public void setUang(BigDecimal Uang) {
        this.Uang = Uang;
    }

    private String noPlat;
    private String jenisKendaraan;
    private String Merk;
    private String jenisBensin;
    private LocalDateTime waktuMasuk;
    private BigDecimal Biaya;
    private int Liter;
    private BigDecimal Uang;
   
    
    public SPBU () {
}
    public SPBU (String noPlat, String jenisKendaraan, String Merk, String jenisBensin, int Liter, BigDecimal Biaya){
        this.noPlat = noPlat;
        this.jenisKendaraan = jenisKendaraan;
        this.Merk = Merk;
        this.jenisBensin = jenisBensin;
        this.Liter = Liter;
        this.Biaya = Biaya;
     
       
        
    }
        
    
    public String getNoPlat() {
        return noPlat;
    }

    public void setNoPlat(String noPlat) {
        this.noPlat = noPlat;
    }

    public String getJenisKendaraan() {
        return jenisKendaraan;
    }

    public void setJenisKendaraan(String jenisKendaraan) {
        this.jenisKendaraan = jenisKendaraan;
    }

    public String getMerk() {
        return Merk;
    }

    public void setMerk(String Merk) {
        this.Merk = Merk;
    }

    public String getJenisBensin() {
        return jenisBensin;
    }

    public void setJenisBensin(String jenisBensin) {
        this.jenisBensin = jenisBensin;
    }

    public LocalDateTime getWaktuMasuk() {
        return waktuMasuk;
    }

    public void setWaktuMasuk(LocalDateTime waktuMasuk) {
        this.waktuMasuk = waktuMasuk;
    }

    public BigDecimal getBiaya() {
        return Biaya;
    }

    public void setBiaya(BigDecimal Biaya) {
        this.Biaya = Biaya;
    }

    public int getLiter() {
        return Liter;
    }

    public void setLiter(int Liter) {
        this.Liter = Liter;
    }
    
    @Override
    public String toString() {
        return "SPBU{" + "noPlat=" + noPlat + ", jenis Kendaraan=" + jenisKendaraan + ", Merk="+ Merk + ", jenis Bensin=" + jenisBensin + ", Liter=" + Liter +"Biaya=" + Biaya+'}';
    }
    
    
    
}
