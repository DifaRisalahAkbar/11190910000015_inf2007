/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.SPBU.model;

/**
 *
 * @author USER
 */
public class Info {
    private final String aplikasi = "Aplikasi SPBU";
    private final String version = "Versi 1.2 Beta";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}
