/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class PenambahanMatriks {

    int i, j;

    public int[][] getPenambahanMatriks(int A[][], int B[][], int nBar, int nKol) {
        Scanner in = new Scanner(System.in);
        int[][] C = new int[nBar][nKol];

        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }

        System.out.println("");
        System.out.println("Hasil penjumlahan Array");
        for (i = 0; i < C.length; i++) {
            for (j = 0; j < C[i].length; j++) {
                System.out.print(C[i][j] + "  ");
            }
            System.out.println("");
        }
        return C;
    }

    public static void main(String[] args) {

        int i, j, nBar, nKol;

        PenambahanMatriks app = new PenambahanMatriks();
        Scanner in = new Scanner(System.in);

        System.out.print("masukkan Baris: ");
        nBar = in.nextInt();
        System.out.print("Masukkan kolom: ");
        nKol = in.nextInt();

        int[][] A = new int[nBar][nKol];
        int[][] B = new int[nBar][nKol];
        int[][] C = new int[nBar][nKol];

        System.out.println("Input nilai Array A");
        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A[i].length; j++) {
                System.out.print("Masukkan Array A [" + i + "," + j + "] : ");
                A[i][j] = in.nextInt();
            }
        }

        System.out.println("\nInput nilai Array B");
        for (i = 0; i < B.length; i++) {
            for (j = 0; j < B[i].length; j++) {
                System.out.print("Masukkan Array B [" + i + "," + j + "] : ");
                B[i][j] = in.nextInt();
            }
        }
        app.getPenambahanMatriks(A, B, nBar, nKol);
    }
}
