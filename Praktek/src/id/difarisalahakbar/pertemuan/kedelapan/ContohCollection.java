/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kedelapan;

import java.util.*;
/**
 *
 * @author USER
 */
public class ContohCollection {
    public static void main(String[] args) {
        
        System.out.println("--- List ---");
        List namaList = new ArrayList();       
        namaList.add(1);
        namaList.add(1);
        namaList.add(2);      
        for (int i = 0; i < namaList.size(); i++) {
            System.out.println(namaList.get(i));
        }
        
        System.out.println("--- set ---");
        Set namaSet = new HashSet();
        namaSet.add(1);
        namaSet.add(1);
        namaSet.add(2);       
        for (Object o : namaSet) {
            System.out.println(o.toString());
        }
        
        System.out.println("--- Map ---");
        Map namaMap = new HashMap();
        namaMap.put(0, "1");
        namaMap.put(1, "1");
        namaMap.put(2, "2");
        namaMap.forEach((k, v) -> {
            System.out.println("key : " + k + " value: " + v);           
        });
    }
}
