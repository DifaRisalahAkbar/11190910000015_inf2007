/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class SequentialSearchLast {
    public int getSequentialSearchOutLast(int L[], int n, int x) {
        int i;

        i = n - 1;
        while ((i > 0) && (L[i] != x)) {
            i = i - 1;
        }
        if (L[i] == x) {
            return i;
        } else {
            return -1;
        }
    }
    
    public int getSequentialSearchInLast(int L[], int n, int x) {
        int i;
        boolean ketemu;

        i = n - 1;
        ketemu = false;
        while ((i >= 0) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            } else {
                i = i - 1;
            }
        }

        if (ketemu) {
            return i;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
        int x;
        int n = 6;

        SequentialSearchLast app = new SequentialSearchLast();
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan x: ");
        x = in.nextInt();

        System.out.println("idx = " + app.getSequentialSearchOutLast(L, n, x));
        //System.out.println("idx = " + app.getSequentialSearchInLast(L, n, x));

    }
}
