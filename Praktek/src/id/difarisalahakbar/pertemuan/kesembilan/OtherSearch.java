/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class OtherSearch {
    public int getOtherSearch (int L[], int x) {
        int i, idx = -1;
        
        for (i = 0; i < L.length; i++) {
            if (L[i] == x) {
                idx = i;
            }           
        }
        return idx;
    }
    
    public static void main(String[] args) {
        int[] L = {13, 16, 14, 21, 76, 15};
        int x, n = 6;
        
        Scanner in = new Scanner(System.in);
        OtherSearch app = new OtherSearch();
        
        System.out.print("masukkan x: ");
        x = in.nextInt();
        
        System.out.println("idx = " + app.getOtherSearch(L, x));
    }
}
