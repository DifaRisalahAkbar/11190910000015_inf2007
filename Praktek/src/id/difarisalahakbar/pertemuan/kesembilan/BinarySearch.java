/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class BinarySearch {

    public int getBinarySearch(int L[], int n, int x) {
        int i, j, k = 0;
        boolean ketemu;

        i = 0;
        j = n - 1;
        ketemu = false;

        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] > x) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int[] L = {81, 76, 21, 18, 16, 13, 10, 7};
        int x;
        int n = 8;

        BinarySearch app = new BinarySearch();
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan x: ");
        x = in.nextInt();

        System.out.println("idx = " + app.getBinarySearch(L, n, x));
    }
}
