/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class StringSearch {
    
    public int getStringSequentialSearch(String L[], int n, String x) {
        int i;

        i = 0;
        while ((i < n - 1) && (!(L[i].equals(x)))) {
            i = i + 1;
        }
        if (L[i].equals(x)) {
            return i;
        } else {
            return -1;
        }
    }
    
    public int getStringBinarySearch(String L[], int n, String x) {
        int i, j, k = 0;
        boolean ketemu;

        i = 0;
        j = n-1;
        ketemu = false;

        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k].equals(x)) {
                ketemu = true;
            } else {
                if (L[k].compareTo(x) < 0) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }
    
    public static void main(String[] args) {
        String[] L = {"A", "B", "C", "D", "E", "F"};
        int n = 6;
        String x;
        
        Scanner in = new Scanner(System.in);
        StringSearch app = new StringSearch();
        
        System.out.print("masukkan karakter: ");
        x = in.next();
        
        System.out.println("idx = " + app.getStringSequentialSearch(L, n, x));
        //System.out.println("idx = " + app.getStringBinarySearch(L, n, x));
    }
}
