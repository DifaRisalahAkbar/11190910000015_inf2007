/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.ketiga;

/**
 *
 * @author USER
 */
import java.util.Scanner;

public class SelisihDuaTanggal {

    public static void main(String[] args) {
        int tanggal1, bulan1, tahun1, tanggal2, bulan2, tahun2, tanggal3, bulan3, tahun3, selisih;
        Scanner in = new Scanner(System.in);
        System.out.print("(input) tanggal 1 : ");
        tanggal1 = in.nextInt();
        System.out.print("(input) bulan 1 : ");
        bulan1 = in.nextInt();
        System.out.print("(input) tahun 1 : ");
        tahun1 = in.nextInt();
        System.out.print("(input) tanggal 2 : ");
        tanggal2 = in.nextInt();
        System.out.print("(input) bulan 2 : ");
        bulan2 = in.nextInt();
        System.out.print("(input) tahun 2 : ");
        tahun2 = in.nextInt();

        selisih = (tahun2 - tahun1) * 365 + (bulan2 - bulan1) * 30 + (tanggal2 - tanggal1);
        tahun3 = selisih / 365;
        bulan3 = (selisih % 365) / 30;
        tanggal3 = (selisih % 365) % 30;

        System.out.println("selisih");
        System.out.println(tahun3 + " tahun " + bulan3 + " bulan " + tanggal3 + " hari");
    }
}
