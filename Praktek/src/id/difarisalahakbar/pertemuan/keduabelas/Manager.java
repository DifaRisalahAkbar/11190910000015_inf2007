/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keduabelas;

/**
 *
 * @author USER
 */
public class Manager extends Pegawai {

    private int tunjangan;

    public Manager(String nama, int gaji, int tunjangan) {
        this.nama = nama;
        this.gaji = gaji;
        this.tunjangan = tunjangan;
    }

    public int infoGaji() {
        return this.gaji;
    }

    public int infoTunjangan() {
        return this.tunjangan;
    }
}
