/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keduabelas;

/**
 *
 * @author USER
 */
public class Programmer extends Pegawai {

    private int bonus;
    private int tunjangan;
    private int gaji;

    public Programmer(String nama, int gaji, int tunjangan) {
        this.nama = nama;
        this.gaji = gaji;
        this.tunjangan = tunjangan;
    }

    public int infoGaji() {
        return this.gaji;
    }

    public int infoBonus() {
        return this.bonus + tunjangan;
    }
}
