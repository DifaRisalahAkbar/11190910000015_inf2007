/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keduabelas;

/**
 *
 * @author USER
 */
public class Bayaran {

     public int hitungBayaran(Pegawai pegawai){
       int uang = pegawai.infoGaji();
       if (pegawai instanceof Manager){
           uang += (((Manager) pegawai).infoTunjangan());
       }else if (pegawai instanceof Programmer){
           uang += ((Programmer) pegawai).infoBonus();
       }
       return uang;
   } 
    public static void main(String[] args) {
        Manager m = new Manager("Joko", 900, 50);
        Programmer p = new Programmer("Mamang", 700, 30);
        Bayaran upah = new Bayaran();
        System.out.println("Upah manager : " + upah.hitungBayaran(m));
        System.out.println("Upah programmer : " + upah.hitungBayaran(p));
    }
}
