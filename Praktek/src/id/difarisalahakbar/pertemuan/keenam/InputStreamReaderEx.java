/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author USER
 */
public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader (new
        InputStreamReader (System.in));
        
        System.out.print("Masukkan bilangan: ");
        try {
            bilangan = Integer.parseInt (in.readLine());
            
            System.out.println("bilangan: " + bilangan);
        } catch (IOException ex) {
            System.out.println("error: " + ex.toString());
        }
    }
}
