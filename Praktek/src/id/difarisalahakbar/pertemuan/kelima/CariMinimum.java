/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class CariMinimum {

    public static void main(String[] args) {
        int n, x, min, i;

        Scanner in = new Scanner(System.in);
        System.out.print("n = ");
        n = in.nextInt();
        System.out.print("bilangan = ");
        x = in.nextInt();
        min = x;
        for (i = 1; i < n; i++) {
            System.out.print("bilangan = ");
            x = in.nextInt();
            if (x < min) {
                min = x;
            }
        }
        System.out.println("nilai terkecil = " + min);
    }
}
