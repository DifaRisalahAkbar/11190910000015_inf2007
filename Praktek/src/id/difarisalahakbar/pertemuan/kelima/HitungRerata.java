/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class HitungRerata {

    public static void main(String[] args) {
        int i, x, jumlah;
        float rerata;

        jumlah = 0;
        i = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("x = ");
        x = in.nextInt();

        while (x != -1) {
            i++;
            jumlah = jumlah + x;
            System.out.print("x = ");
            x = in.nextInt();
        }
        if (i != 0) {
            rerata = (float) jumlah / i;
            System.out.println("rata-rata = " + rerata);
        } else {
            System.out.println("tidak ada nilai ujian yang dimasukkan");
        }
    }
}
