/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kedua;

/**
 *
 * @author Mahasiswa
 */
public class Divide {
    public static void main(String[] arguments) {
        float number1 = 15;
        float number2 = 6;
        float result = number1 / number2;
        float reminder = number1 % number2;
        System.out.println(number1 + " divided by " + number2);
        System.out.println("\nResult\tReminder");
        System.out.println(" "+result + "\t" + "  " +reminder);
    }
}
