/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kedua;

/**
 *
 * @author Mahasiswa
 */
public class Invest {
    public static void main(String[] arguments) {
        float total = 14000;
        System.out.println("Original investment: $" + total);
        
        // Increases by 40 percent the first year
        total = total + (total * .4F);
        System.out.println("After two years: $" + total);
        
        // Loses $1,500 the second year
        total = total - 1500F;
        System.out.println("After two years: $" + total);
        
        //increases by 12 percent the third year
        total = total + (total * .12F);
        System.out.println("After three years: $" + total);
    }
}
