/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class AplikasiKabisat {

    public static void main(String[] args) {
        int tahun;

        Scanner in = new Scanner(System.in);
        System.out.print("tahun: ");
        tahun = in.nextInt();

        Kabisat kabisat = new Kabisat();
        if (kabisat.getHasil(tahun)) {
            System.out.println("tahun kabisat");
        } else {
            System.out.println("bukan tahun kabisat");
        }
    }
}
