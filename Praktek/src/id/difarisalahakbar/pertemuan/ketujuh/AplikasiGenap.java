/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class AplikasiGenap {

    public static void main(String[] args) {
        int bilangan;

        Scanner in = new Scanner(System.in);
        System.out.print("bilangan: ");
        bilangan = in.nextInt();

        Genap genap = new Genap();
        if (genap.getHasil(bilangan)) {
            System.out.println("Genap");
        } else {
            System.out.println("Ganjil");
        }
    }
}
