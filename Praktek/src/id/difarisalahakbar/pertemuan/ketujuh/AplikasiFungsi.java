/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.ketujuh;

/**
 *
 * @author USER
 */
public class AplikasiFungsi {

    public static void main(String[] args) {
        float x;
        Fungsi fungsi = new Fungsi();

        System.out.println("_________________________");  
        System.out.println("");
        System.out.println("|   x     |     f(x)   |");
        System.out.println("_________________________");

        x = 10;
        while (x <= 15.0) {
            System.out.println( x + "       " + fungsi.getHasil(x));
            x = (float)(x + 0.2);
        }
        System.out.println("_________________________");
    }
}
