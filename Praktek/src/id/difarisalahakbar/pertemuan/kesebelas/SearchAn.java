/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class SearchAn {

    boolean getCariKataAn(String file) {
        boolean ketemu = false;
        char[] L = new char[100];
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            Scanner line = new Scanner(br);
            while (line.hasNext()) {
                L = line.next().toCharArray();
                for (int i = 0; i < L.length - 1; i++) {
                    if (L[i] == 'a') {
                        if (L[i + 1] == 'n') {
                            ketemu = true;
                        }
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArsipTeks.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ketemu;
    }

    public static void main(String[] args) {
        String Kalimat;
        String File = "C:\\tonight\\Arsip_An.txt";
        Scanner in = new Scanner(System.in);
        SearchAn baca = new SearchAn();
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream(File));

            System.out.print("Masukkan Kalimat: ");
            Kalimat = in.nextLine();
            
            outFile.println(Kalimat);
            outFile.close();
            
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
        
        System.out.println("\nApakah terdapat 'an'? " + baca.getCariKataAn(File));
    }
}
