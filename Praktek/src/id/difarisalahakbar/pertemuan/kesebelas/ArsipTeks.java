/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author USER
 */
public class ArsipTeks {

    public int getHitungKarakter(FileReader T) {
        char[] C;
        int n;

        n = 0;
        
        Scanner line = new Scanner(new BufferedReader(T));
        while (line.hasNext()) {
            C = line.next().toCharArray();
            for (char d : C) {
                if (d == 'a') {
                    n = n + 1;
                }
            }
        }
        return n;
    }

    public static void main(String[] args) {
        int a;
        String Arsip = "C:\\tonight\\ArsipTeks.txt";
        Scanner in = new Scanner(System.in);
        ArsipTeks app = new ArsipTeks();

        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("C:\\Tonight\\ArsipTeks.txt"));
            outFile.println("ini adalah program menghitung huruf a dari teks");
            outFile.close();
            
            System.out.println(app.getHitungKarakter(new FileReader("C:\\Tonight\\ArsipTeks.txt")));
            
        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}
