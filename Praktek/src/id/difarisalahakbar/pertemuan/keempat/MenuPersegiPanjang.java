/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author USER
 */
public class MenuPersegiPanjang {
    public static void main(String[] args) {
        int luas,keliling,diagonal,panjang,lebar;
        int NomorMenu;
        
        Scanner in = new Scanner(System.in);
        System.out.println("Ini adalah rumusan mengitung persegi panjang");
        System.out.println("1. Mengitung Luas");
        System.out.println("2. Menghitung Keliling");
        System.out.println("3. Mengitung Diagonal");
        System.out.println("4. Batal");
        System.out.println("Pilih antara 1-3 atau 4 jika anda ingin keluar dari program.");
        NomorMenu = in.nextInt();
        
        switch(NomorMenu){
            case 1:
                System.out.println("Masukan Nilai Panjang");
                panjang = in.nextInt();
                System.out.println("Masukan Nilai Lebar");
                lebar = in.nextInt();
                luas = panjang*lebar;
                System.out.println("Luas Persegi Panjang dengan panjang" + " "+panjang + " " + "dan lebar" + " "+lebar+ " " + "adalah" +" "+luas);
                break;
                
            case 2:
                System.out.println("Masukan Nilai Panjang");
                panjang = in.nextInt();
                System.out.println("Masukan Nilai Lebar");
                lebar = in.nextInt();
                keliling = 2*panjang + 2*lebar;
                System.out.println("Keliling Persegi Panjang dengan panjang" + " "+panjang + " "+ "dan lebar" + " "+lebar+" "+"adalah" +" "+keliling);
                break;
                
            case 3:
                System.out.println("Masukan Nilai Panjang");
                panjang = in.nextInt();
                System.out.println("Masukan Nilai Lebar");
                lebar = in.nextInt();
                diagonal = panjang*panjang + lebar*lebar;
                System.out.println("Diagonal Persegi Panjang dengan panjang" + " "+panjang + " "+ "dan lebar" + " "+lebar+" "+"adalah" +" "+diagonal);
                break;
                
            case 4:
                System.out.println("Well, have a nice day !");
                break;
                
            default:
                System.out.println("You input the one wrong...[failed]");
                
        }
    }
}
