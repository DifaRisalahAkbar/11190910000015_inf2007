/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.difarisalahakbar.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author USER
 */
public class KonversiAngkaKeTeks {
    public static void main(String[] args) {
        int angka;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukan Angka");
        angka = in.nextInt();
        
        switch(angka){
            case 1:
                System.out.println("satu");
                break;
            case 2:
                System.out.println("dua");
                break;
            case 3:
                System.out.println("tiga");
                break;
            case 4:
                System.out.println("empat");
                break;
            default:
                System.out.println("Angka yang dimasukan salah!");
        }
    }
}
